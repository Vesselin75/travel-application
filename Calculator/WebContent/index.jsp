<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="./css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./css/calculator.css">
</head>
<body>	
    <%
		String pageName = (String)session.getAttribute("pageName");
	%>
	<div class="container">
  		<div class="row">
    		<div class="col-md-8">
    			<jsp:include page="<%= pageName %>" />
    		</div>
  		</div>
	</div>
</body>
</html>