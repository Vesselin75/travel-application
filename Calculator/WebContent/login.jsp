<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ page import="calculator.CalculatorFields" %>

<%
	CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
	String login_username = calcFields.getFieldVal(CalculatorFields.LOGIN_USERNAME);
	String login_password = calcFields.getFieldVal(CalculatorFields.LOGIN_PASSWORD);
	
	String usernameLoginErr = (String)session.getAttribute("usernameLoginErr");
	if (usernameLoginErr == null) {
		usernameLoginErr = "";
	}
	String passwordLoginErr = (String)session.getAttribute("passwordLoginErr");
	if (passwordLoginErr == null) {
		passwordLoginErr = "";
	}
%>
<form class="form-horizontal" method="post" action="/calculator/calc?a=login">
  <div class="form-group">
    <label for="username" class="col-sm-4 control-label">Username</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="login_username" id="login_username" value="<%= login_username %>" placeholder="Username">
      <span class="calc_error"><%= usernameLoginErr %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" name="login_password" id="login_password" value="<%= login_password %>" placeholder="Password">
      <span class="calc_error"><%= passwordLoginErr %></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-5">
      <button type="submit" class="btn btn-default" style="margin-right: 10px">Sign in</button>      
      <a class="btn btn-default" href="/calculator/calc?a=register" role="button">Register</a>
    </div>
  </div>
</form>