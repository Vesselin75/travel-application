<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ page import="calculator.CalculatorFields" %>

<%
	CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
	String register_username = calcFields.getFieldVal(CalculatorFields.REGISTER_USERNAME);
	String register_password = calcFields.getFieldVal(CalculatorFields.REGISTER_PASSWORD);
	String fullname = calcFields.getFieldVal(CalculatorFields.REGISTER_FULLNAME);
	String email = calcFields.getFieldVal(CalculatorFields.REGISTER_EMAIL);

	String usernameExistsError = (String)session.getAttribute("usernameExistsError");
	if (usernameExistsError == null) {
		usernameExistsError = "";
	}
	String passwordLengthError = (String)session.getAttribute("passwordLengthError");
	if (passwordLengthError == null) {
		passwordLengthError = "";
	}
	String fullnameError = (String)session.getAttribute("fullnameError");
	if (fullnameError == null) {
		fullnameError = "";
	}	
	String emailError = (String)session.getAttribute("emailError");
	if (emailError == null) {
		emailError = "";
	}
	
%>
<form class="form-horizontal" method="post" action="/calculator/calc?a=register">
  <div class="form-group">
    <label for="username" class="col-sm-4 control-label">Username</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="register_username" id="register_username" value="<%= register_username %>" placeholder="Username">
      <span class="calc_error"><%= usernameExistsError %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-4 control-label">Password (8 symbols)</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" name="register_password" id="register_password" value="<%= register_password %>" placeholder="Password">
      <span class="calc_error"><%= passwordLengthError %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="fullname" class="col-sm-4 control-label">Full Name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="fullname" id="fullname" value="<%= fullname %>" placeholder="Full Name">
      <span class="calc_error"><%= fullnameError %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-5">
      <input type="email" class="form-control" name="email" id="email" value="<%= email %>" placeholder="Email">
      <span class="calc_error"><%= emailError %></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-5">
      <button type="submit" class="btn btn-default">Register</button>
    </div>
  </div>
</form>