<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1" %>

<%@ page import="calculator.CalculatorException" %>

<%
	CalculatorException calculatorException = (CalculatorException)session.getAttribute("calculatorException");
	String calculatorExceptionError = calculatorException.getMessage();
%>

<span class="calc_error"><%= calculatorExceptionError %></span>