<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ page import="calculator.op.CalculatorOpHistory" %>
<%@ page import="calculator.op.CalculatorOp" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>

<form class="form-horizontal">
    <div class="form-group">
    	<div class="col-sm-10">
			<table class="table">
			  <thead>
			    <tr>
			      <th>No.</th>
			      <th>Operand 1</th>
			      <th>Operand 2</th>
			      <th>Result</th>
			      <th>Time</th>
			      <th>Type</th>
			    </tr>
			  </thead>
			  <tbody>
			  	  <% 
			  	  CalculatorOpHistory history = (CalculatorOpHistory)session.getAttribute("history");
			  	  HashMap<Integer, CalculatorOp> historyOps = history.getOperations();
			  	  Iterator<Integer> iterator = historyOps.keySet().iterator();
			  	  while (iterator.hasNext()) {
			  	  	  Integer nextIndex = (Integer)iterator.next();
			  	  	  CalculatorOp calculatorOp = (CalculatorOp)historyOps.get(nextIndex);
			  	  %>	
			  	  <tr>
			        <td><%= nextIndex.intValue() %></td>
			        <td><%= calculatorOp.getOperand_1() %></td>
			        <td><%= calculatorOp.getOperand_2() %></td>
			        <td><%= calculatorOp.getOperationResult() %></td>
			        <td><%= calculatorOp.getFormattedTime() %></td>
			        <td><%= calculatorOp.getOperationTypeName() %></td>
			      </tr>
			      <% } %>
			  </tbody>
			</table>
		</div>
	</div>	
	<div class="form-group">
	    <div class="col-sm-offset-0 col-sm-10">
	        <a class="btn btn-default" href="/calculator/calc?a=calculator" role="button">Go To Calculator</a>	
	    </div>
    </div>
</form>