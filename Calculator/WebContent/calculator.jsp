<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ page import="calculator.CalculatorFields" %>

<%
	CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
	String operand_1 = calcFields.getFieldVal(CalculatorFields.CALULATOR_OPERAND1);
	String operand_2 = calcFields.getFieldVal(CalculatorFields.CALULATOR_OPERAND2);
	String operation_type = calcFields.getFieldVal(CalculatorFields.CALULATOR_OPERATION_TYPE);
	String result = calcFields.getFieldVal(CalculatorFields.CALULATOR_RESULT);

	String addition_selected = "";
	String subtraction_selected = "";
	String multiplication_selected = "";
	String division_selected = "";
	if (operation_type.equals("1")) {
		addition_selected = "selected";
	}
	if (operation_type.equals("2")) {
		subtraction_selected = "selected";
	}
	if (operation_type.equals("3")) {
		multiplication_selected = "selected";
	}
	if (operation_type.equals("4")) {
		division_selected = "selected";
	}
	
	String operand1Err = (String)session.getAttribute("operand1Err");
	if (operand1Err == null) {
		operand1Err = "";
	}
	String operand2Err = (String)session.getAttribute("operand2Err");
	if (operand2Err == null) {
		operand2Err = "";
	}
	String divisionByZeroErr = (String)session.getAttribute("divisionByZeroErr");
	if (divisionByZeroErr == null) {
		divisionByZeroErr = "";
	}
%>
<form class="form-horizontal" method="post" action="/calculator/calc?a=calculator">
  <div class="form-group">
    <label for="operand_1" class="col-sm-4 control-label">Operand 1</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="operand_1" id="operand_1" value="<%= operand_1 %>" placeholder="Operand 1">
      <span class="calc_error"><%= operand1Err %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="operand_2" class="col-sm-4 control-label">Operand 2</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="operand_2" id="operand_2" value="<%= operand_2 %>" placeholder="Operand 2">
      <span class="calc_error"><%= operand2Err %></span><br/>
  	  <span class="calc_error"><%= divisionByZeroErr %></span>
    </div>
  </div>
  <div class="form-group">
    <label for="operation_type" class="col-sm-4 control-label">Operation Type</label>
    <div class="col-sm-5">
      <select class="form-control" name="operation_type" id="operation_type">
  		<option value="1" <%= addition_selected %> >ADDITION</option>
  		<option value="2" <%= subtraction_selected %> >SUBTRACTION</option>
  		<option value="3" <%= multiplication_selected %> >MULTIPLICATION</option>
  		<option value="4" <%= division_selected %> >DIVISION</option>
	  </select>	
    </div>
  </div>
  <div class="form-group">
    <label for="result" class="col-sm-4 control-label">Result</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="result" id="result" value="<%= result %>" placeholder="Result" readonly>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
      <button type="submit" class="btn btn-default" style="margin-right: 10px">Calculate</button>
      <a class="btn btn-default" href="/calculator/calc?a=history" role="button" style="margin-right: 10px">View History</a>
      <a class="btn btn-default" href="/calculator/calc?a=logout" role="button">Logout</a>
    </div>
  </div>
</form>