package calculator.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CalculatorException;
import calculator.CalculatorFacade;
import calculator.CalculatorServlet;
import calculator.CalculatorUser;

/** Action for Logout
 * 
 * @author Vesselin75
 *
 */
public class LogoutAction extends CalculatorBaseAction {

	@Override
	public void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		CalculatorUser calcUser = (CalculatorUser)session.getAttribute("calcUser");
		try {
			CalculatorFacade.logoutUser(calcUser);
			session.invalidate();
		} catch (CalculatorException calcex) {
			servlet.redirectError(calcex, req, resp);
		}
		servlet.redirect(CalculatorBaseAction.ACTION_LOGIN, req, resp);	
	}

}
