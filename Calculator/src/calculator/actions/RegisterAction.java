package calculator.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CalculatorException;
import calculator.CalculatorFacade;
import calculator.CalculatorFields;
import calculator.CalculatorServlet;
import calculator.CalculatorUser;
import calculator.validation.CalculatorValidationMessages;
import calculator.validation.EmailValidator;
import calculator.validation.NameValidator;
import calculator.validation.PasswordValidator;

/** Action for registration
 * 
 *  @author Vesselin75
 *
 */
public class RegisterAction extends CalculatorBaseAction {

	@Override
	public void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String register_username = req.getParameter("register_username");
		String register_password = req.getParameter("register_password");
		String fullname = req.getParameter("fullname");
		String email = req.getParameter("email");
		CalculatorUser calcUser = new CalculatorUser();
		calcUser.setUsername(register_username);
		calcUser.setPassword(register_password);
		calcUser.setFullname(fullname);
		calcUser.setEmail(email);
		boolean hasErrors = false;
		HttpSession session = req.getSession(true);
		//load parameters in session cache
		this.loadParamsInCache(session, register_username, register_password, fullname, email);
		//clean errors
		this.cleanErrors(session);
		//check username exists
		try {
			boolean isTaken = CalculatorFacade.checkUsernameExists(register_username);
			if (isTaken == true) {
				session.setAttribute("usernameExistsError", CalculatorValidationMessages.getMessage(3));
				hasErrors = true;
			}	
		} catch (CalculatorException calcex) {
			servlet.redirectError(calcex, req, resp);
		}
		//validate password length - 8 symbols
		boolean isPasswordCorrect = PasswordValidator.validate(register_password);
		if (isPasswordCorrect == false) {
			session.setAttribute("passwordLengthError", CalculatorValidationMessages.getMessage(4));
			hasErrors = true;
		}
		//validate full name - 2 strings
		boolean isNameCorrect = NameValidator.validate(fullname);
		if (isNameCorrect == false) {
			session.setAttribute("fullnameError", CalculatorValidationMessages.getMessage(5));
			hasErrors = true;
		}
		//validate email
		EmailValidator emailValidator = new EmailValidator();
		boolean isEmailValid = emailValidator.validate(calcUser.getEmail());
		if (isEmailValid == false) {
			session.setAttribute("emailError", CalculatorValidationMessages.getMessage(6));
			hasErrors = true;
		}
		if (hasErrors == true) {
			servlet.redirect(CalculatorBaseAction.ACTION_REGISTER, req, resp);
		} else {
			//register user - only if no errors
			try {
				CalculatorFacade.registerUser(calcUser);
			} catch (CalculatorException calcex) {
				servlet.redirectError(calcex, req, resp);
			}
			//redirect to login
			servlet.redirect(CalculatorBaseAction.ACTION_LOGIN, req, resp);
		}
	}
	
	/** Clean errors before validation
	 * 
	 * @param session
	 */
	private void cleanErrors (HttpSession session) {
		session.removeAttribute("usernameExistsError");
		session.removeAttribute("passwordLengthError");
		session.removeAttribute("fullnameError");
		session.removeAttribute("emailError");
	}
	
	/** Loads parameters in session cache 
	 * 
	 * @param session
	 * @param register_username
	 * @param register_password
	 * @param fullname
	 * @param email
	 */
	private void loadParamsInCache(HttpSession session, String register_username, String register_password, String fullname, String email) {
		CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
		calcFields.setFieldVal(CalculatorFields.REGISTER_USERNAME, register_username);
		calcFields.setFieldVal(CalculatorFields.REGISTER_PASSWORD, register_password);
		calcFields.setFieldVal(CalculatorFields.REGISTER_FULLNAME, fullname);
		calcFields.setFieldVal(CalculatorFields.REGISTER_EMAIL, email);
		session.setAttribute("calcFields", calcFields);
	}


}
