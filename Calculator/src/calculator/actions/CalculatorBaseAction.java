package calculator.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import calculator.CalculatorServlet;

/** Generic Action
 * 
 * @author Vesselin75
 *
 */
public abstract class CalculatorBaseAction {
	
	/** Servlet Actions */
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_REGISTER = "register";
	public static final String ACTION_CALCULATOR = "calculator";
	public static final String ACTION_HISTORY = "history";	
	public static final String ACTION_LOGOUT = "logout";
	public static final String ACTION_ERROR = "error";

	public abstract void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
	
}
