package calculator.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CalculatorException;
import calculator.CalculatorFacade;
import calculator.CalculatorServlet;
import calculator.CalculatorUser;
import calculator.op.CalculatorOpHistory;

public class HistoryAction extends CalculatorBaseAction {

	@Override
	public void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		CalculatorUser calcUser = (CalculatorUser)session.getAttribute("calcUser");
		try {
			CalculatorOpHistory calculatorOpHistory = CalculatorFacade.getOpHistory(calcUser);
			session.setAttribute("history", calculatorOpHistory);				
		} catch (CalculatorException calcex) {
			servlet.redirect(CalculatorBaseAction.ACTION_ERROR, req, resp);
		}
		servlet.redirect(CalculatorBaseAction.ACTION_HISTORY, req, resp);
	}

}
