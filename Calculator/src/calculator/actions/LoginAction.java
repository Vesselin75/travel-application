package calculator.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CalculatorException;
import calculator.CalculatorFacade;
import calculator.CalculatorFields;
import calculator.CalculatorServlet;
import calculator.CalculatorUser;
import calculator.validation.CalculatorValidationMessages;

/** Action for Login
 * 
 * @author Vesselin75
 *
 */
public class LoginAction extends CalculatorBaseAction {

	@Override
	public void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login_username = req.getParameter("login_username");
		String login_password = req.getParameter("login_password");
		HttpSession session = req.getSession(true);
		//load parameters in session cache
		this.loadParamsInCache(session, login_username, login_password);		
		//clean errors
		this.cleanErrors(session);
		try {
			boolean isUsernameValid = true;
			boolean isPasswordValid = true;
			//validate username
			try {
				isUsernameValid = CalculatorFacade.checkUsernameExists(login_username);
				if (isUsernameValid == false) {
					session.setAttribute("usernameLoginErr", CalculatorValidationMessages.getMessage(1));					
				}
			} catch (CalculatorException calcex) {
				servlet.redirectError(calcex, req, resp);
			}
			//validate password
			try {
				if (isUsernameValid == true) {
					isPasswordValid = CalculatorFacade.checkPassword(login_username, login_password);
					if (isPasswordValid == false) { 
						session.setAttribute("passwordLoginErr", CalculatorValidationMessages.getMessage(2));
					}
				}	
			} catch (CalculatorException calcex) {
				servlet.redirectError(calcex, req, resp);
			}
			if ( (isUsernameValid == false) || (isPasswordValid == false) ) {
				servlet.redirect(CalculatorBaseAction.ACTION_LOGIN, req, resp);
			} else {
				//get user object
				CalculatorUser calcUser = CalculatorFacade.getUser(login_username);
				//log user
				CalculatorFacade.loginUser(calcUser);
				//add logged user to session
				session.setAttribute("calcUser", calcUser);
				//redirect to calculator
				servlet.redirect(CalculatorBaseAction.ACTION_CALCULATOR, req, resp);
			}
		} catch (CalculatorException calcex) {
			servlet.redirectError(calcex, req, resp);
		}	
	}
	
	/** Clean errors before validation
	 * 
	 * @param session
	 */
	private void cleanErrors (HttpSession session) {
		session.removeAttribute("usernameLoginErr");
		session.removeAttribute("passwordLoginErr");
	}
	
	/** Loads paramters in cache
	 * 
	 * @param session - session
	 * @param login_username username
	 * @param login_password password
	 */
	private void loadParamsInCache(HttpSession session, String login_username, String login_password) {
		CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
		calcFields.setFieldVal(CalculatorFields.LOGIN_USERNAME, login_username);
		calcFields.setFieldVal(CalculatorFields.LOGIN_PASSWORD, login_password);	
		session.setAttribute("calcFields", calcFields);
	}

}
