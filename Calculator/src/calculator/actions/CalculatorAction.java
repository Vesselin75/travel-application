package calculator.actions;
	
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CalculatorException;
import calculator.CalculatorFacade;
import calculator.CalculatorFields;
import calculator.CalculatorServlet;
import calculator.CalculatorUser;
import calculator.op.Calculator;
import calculator.op.CalculatorOp;
import calculator.validation.CalculatorValidationMessages;
import calculator.validation.OperandValidator;

public class CalculatorAction extends CalculatorBaseAction {

	@Override
	public void handleAction(CalculatorServlet servlet, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String operand_1 = (String)req.getParameter("operand_1");
		String operand_2 = (String)req.getParameter("operand_2");
		String operation_type_str = (String)req.getParameter("operation_type");
		int operation_type = Integer.parseInt(operation_type_str);
		boolean hasErrors = false;
		HttpSession session = req.getSession(true);
		//load fields in session cache
		this.loadParamsInCache(session, operand_1, operand_2, operation_type_str);
		//clean errors
		this.cleanErrors(session);
		//validate operand_1
		boolean isOperand1Valid = OperandValidator.validateOperand(operand_1);
		if (isOperand1Valid == false) {
			session.setAttribute("operand1Err", CalculatorValidationMessages.getMessage(7));
			hasErrors = true;
		}
		//validate operand_2
		boolean isOperand2Valid = OperandValidator.validateOperand(operand_2);
		if (isOperand2Valid == false) {
			session.setAttribute("operand2Err", CalculatorValidationMessages.getMessage(7));
			hasErrors = true;
		}
		//validate division by zero operand
		if (operation_type == 4) {
			boolean isOperand2Zero = OperandValidator.isZeroOperand(operand_2);
			if (isOperand2Zero == true) {
				session.setAttribute("divisionByZeroErr", CalculatorValidationMessages.getMessage(8));
				hasErrors = true;
			}	
		}
		//refresh page
		if (hasErrors == true) {
			servlet.redirect(CalculatorBaseAction.ACTION_CALCULATOR, req, resp);
		} else {
			//convert to double is ok 
			double operand_1_dbl = Double.parseDouble(operand_1);
			double operand_2_dbl = Double.parseDouble(operand_2);
			//calculate
			double result = 0;
			if (operation_type == CalculatorOp.ADDITION) {
				result = Calculator.addOp(operand_1_dbl, operand_2_dbl);
			}
			if (operation_type == CalculatorOp.SUBTRACTION) {
				result = Calculator.subtractOp(operand_1_dbl, operand_2_dbl);
			}
			if (operation_type ==  CalculatorOp.MULTIPLICATION) {
				result = Calculator.multiplyOp(operand_1_dbl, operand_2_dbl);
			}
			if (operation_type == CalculatorOp.DIVISION) {
				result = Calculator.divideOp(operand_1_dbl, operand_2_dbl);
			}
			//load result in session cache
			String resultStr = Double.toString(result);
			this.loadResultInCache(session, resultStr);
			//log operation
			CalculatorOp calcOp = new CalculatorOp();
			CalculatorUser calcUser = (CalculatorUser)session.getAttribute("calcUser");
			calcOp.setUserId(calcUser.getUserId());
			calcOp.setOperand_1(operand_1_dbl);
			calcOp.setOperand_2(operand_2_dbl);
			calcOp.setOperationResult(result);
			Date now = new Date();
			Timestamp nowTime = new Timestamp( now.getTime() );
			calcOp.setOpTime(nowTime);
			calcOp.setOpTypeId(operation_type);
			try {
				CalculatorFacade.logOperation(calcOp);
			} catch (CalculatorException calcex) {
				servlet.redirect(CalculatorBaseAction.ACTION_ERROR, req, resp);
			}
			//refresh calculator
			servlet.redirect(CalculatorBaseAction.ACTION_CALCULATOR, req, resp);
		}
 	}
	
	/** Clean errors before validation
	 * 
	 * @param session
	 */
	private void cleanErrors (HttpSession session) {
		session.removeAttribute("operand1Err");
		session.removeAttribute("operand2Err");
		session.removeAttribute("divisionByZeroErr");
	}
	
	/** Loads parameters in session cache 
	 * 
	 * @param session
	 * @param register_username
	 * @param register_password
	 * @param fullname
	 * @param email
	 */
	private void loadParamsInCache(HttpSession session, String operand_1, String operand_2, String operation_type) {
		CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
		calcFields.setFieldVal(CalculatorFields.CALULATOR_OPERAND1, operand_1);
		calcFields.setFieldVal(CalculatorFields.CALULATOR_OPERAND2, operand_2);
		calcFields.setFieldVal(CalculatorFields.CALULATOR_OPERATION_TYPE, operation_type);
		session.setAttribute("calcFields", calcFields);
	}
	
	/** Load result in cache 
	 * 
	 * @param session
	 * @param result 
	 * 
	 */
	private void loadResultInCache (HttpSession session, String result) {
		CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
		calcFields.setFieldVal(CalculatorFields.CALULATOR_RESULT, result);
		session.setAttribute("calcFields", calcFields);
	}

}
