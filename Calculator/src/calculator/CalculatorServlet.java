package calculator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.actions.CalculatorAction;
import calculator.actions.CalculatorBaseAction;
import calculator.actions.HistoryAction;
import calculator.actions.LoginAction;
import calculator.actions.LogoutAction;
import calculator.actions.RegisterAction;

/** The Main Servlet class used in the MVC pattern
 * 
 *  The servlet is called with parameters
 *  a = action
 *  
 *  a = login - action for login
 *  a = register - action for registration
 *  a = calculator - action for calculator
 *  a = history - action for history
 *   
 * 
 * @author Vesselin75
 *
 */
public class CalculatorServlet extends javax.servlet.http.HttpServlet {

	private static final long serialVersionUID = 1L;
	
	/** Database connection for the mysql database
	 * 
	 */
	public static Connection conn;
	
	/** Pages */
	private static final String PAGE_LOGIN = "login.jsp";
	private static final String PAGE_REGISTER = "register.jsp";
	private static final String PAGE_CALCULATOR = "calculator.jsp";
	private static final String PAGE_HISTORY = "history.jsp";
	private static final String PAGE_ERROR = "error.jsp";
	
	/** Init Method
	 * 
	 */
	public void init() throws ServletException {
		try {
			conn = CalculatorConnection.getConnection();
			CalculatorFacade.conn = conn;
		} catch (CalculatorException calcEx) {
			System.out.println(calcEx.getMessage());
			System.exit(1);
		}
	}
	
	/** Destroy Method
	 * 
	 */
	public void destroy() {
		try {
			conn.close();
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
	}
	
	/** DoGet Method - Navigation
	 * 
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String a = req.getParameter("a");
		//process logout
		if (a.equals(CalculatorBaseAction.ACTION_LOGOUT)) {
			LogoutAction logoutAction = new LogoutAction();
			logoutAction.handleAction(this, req, resp);
		} 
		//process history
		else if (a.equals(CalculatorBaseAction.ACTION_HISTORY)) {
			HistoryAction historyAction = new HistoryAction();
			historyAction.handleAction(this, req, resp);	
		} else { 	
			this.redirect(a, req, resp);
		}	
	}
	
	/** DoPost Method - Login, Calculator, Register
	 * 
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String a = req.getParameter("a");
		//process login 
		if (a.equals(CalculatorBaseAction.ACTION_LOGIN)) {
			LoginAction loginAction = new LoginAction();
			loginAction.handleAction(this, req, resp);
		}
		//process registration
		if (a.equals(CalculatorBaseAction.ACTION_REGISTER)) {
			RegisterAction registerAction = new RegisterAction();
			registerAction.handleAction(this, req, resp);
		}
		//process calculations
		if (a.equals(CalculatorBaseAction.ACTION_CALCULATOR)) {
			CalculatorAction calculatorAction = new CalculatorAction();
			calculatorAction.handleAction(this, req, resp);	
		}
	}
	
	/** Redirects to a page based on action - all inside index.jsp
	 * 
	 * @param a action
	 * @param req request
	 * @param resp response
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	public void redirect(String a, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pageName = "";
		if (a.equals(CalculatorBaseAction.ACTION_LOGIN)) {
			pageName = PAGE_LOGIN;
		}
		if (a.equals(CalculatorBaseAction.ACTION_REGISTER)) {
			pageName = PAGE_REGISTER;
		}
		if (a.equals(CalculatorBaseAction.ACTION_CALCULATOR)) {
			pageName = PAGE_CALCULATOR;
		}
		if (a.equals(CalculatorBaseAction.ACTION_HISTORY)) {
			pageName = PAGE_HISTORY;
		}
		if (a.equals(CalculatorBaseAction.ACTION_LOGOUT)) {
			pageName = PAGE_LOGIN;
		}
		//store the pageName in the session for the index.jsp
		HttpSession session = req.getSession(true);
		session.setAttribute("pageName", pageName);
		//init the calculator fields cache
		CalculatorFields calcFields = (CalculatorFields)session.getAttribute("calcFields");
		if (calcFields == null) {
			calcFields = new CalculatorFields();
			session.setAttribute("calcFields", calcFields);
		}
		//redirect
		RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
		dispatcher.forward(req, resp);
	}
	
	public void redirectError(CalculatorException calculatorException, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pageName = PAGE_ERROR;
		//store the pageName in the session for the index.jsp
		HttpSession session = req.getSession(true);
		session.setAttribute("pageName", pageName);
		//add the exception to session to be used on the error.jsp
		session.setAttribute("calculatorException", calculatorException);
		//redirect
		RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
		dispatcher.forward(req, resp);
	}
	

}
