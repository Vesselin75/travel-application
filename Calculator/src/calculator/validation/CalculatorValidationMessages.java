package calculator.validation;

import java.util.Properties;

/** Exception used for field validation
 * 
 * @author Vesselin75
 *
 */
public class CalculatorValidationMessages {

	private static final Properties validationMessages = new Properties();
	
	static {
		validationMessages.put(new Integer(1),  "Invalid login username.");
		validationMessages.put(new Integer(2),  "Invalid login password.");
		
		validationMessages.put(new Integer(3),  "Username already exists.");
		validationMessages.put(new Integer(4),  "Invalid password length - must be 8 symbols.");
		validationMessages.put(new Integer(5),  "Invalid full name - first and second required.");
		validationMessages.put(new Integer(6),  "Invalid registration email format.");
		
		validationMessages.put(new Integer(7),  "Invalid operand.");
		validationMessages.put(new Integer(8),  "Invalid second operand.");
	}
	
	public CalculatorValidationMessages() {}
	
	public static String getMessage(int code) {
		return (String)validationMessages.get(new Integer(code));
	}
	
}
