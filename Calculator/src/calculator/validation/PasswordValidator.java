package calculator.validation;

/** Validates password - 8 symbols
 * 
 * @author Vesselin75
 *
 */
public class PasswordValidator {

	public PasswordValidator() {}
	
	public static boolean validate(String password) {
		if (password.length() != 8) {
			return false;
		}
		return true;
	}
	
}
