package calculator.validation;

public class OperandValidator {

	public OperandValidator() {}
	
	public static boolean validateOperand(String operand) {
		try {
			Double operandDbl = Double.parseDouble(operand);
		} catch (NumberFormatException numex) {
			return false; 
		}
		return true;
	}
	
	public static boolean isZeroOperand(String operand) {
		double operandDbl = 0;
		try {
			operandDbl = Double.parseDouble(operand);
		} catch (NumberFormatException numex) {
			return false; 
		}
		if (operandDbl == 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
