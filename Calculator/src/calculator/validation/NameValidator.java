package calculator.validation;

import java.util.StringTokenizer;

/** Validates a full name
 * 
 * @author Vesselin75
 *
 */
public class NameValidator {

    public NameValidator() {}
	
	public static boolean validate(String fullname) {
		StringTokenizer tzer = new StringTokenizer(fullname, " ");
		if (tzer.countTokens() != 2) {
			return false;
		}
		while (tzer.hasMoreTokens()) {
			String token = tzer.nextToken();
			if (NameValidator.isLetters(token) != true) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isLetters(String name) {
	    char[] chars = name.toCharArray();
	    for (char c : chars) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }
	    return true;
	}
	
}
