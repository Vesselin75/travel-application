package calculator;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/** Database Connectivity Interface
 * 
 * 	Has properties file - calculator.properties where the db settings are.
 *  Has a getConnection() static method which checks for initialisation.
 *  The database connection is stored in a static variable and is returned if already initialised.   
 *  
 * @author Vesselin75
 *
 */
public class CalculatorConnection {

	private static String HOST_NAME = "";
	private static String PORT_NAME = "";
	private static String MYSQL_USER = "";
	private static String MYSQL_PASSWORD = "";
	private static String SCHEMA_NAME = "";
	
	private static Connection calculatorConn;
	private static Properties calculatorProps;
	
	public static Connection getConnection() throws CalculatorException {
		//load the properties
		if (calculatorProps == null) {
			try {
				calculatorProps = new Properties();
				InputStream is = CalculatorConnection.class.getClassLoader().getResourceAsStream("calculator.properties");
				calculatorProps.load(is);
				CalculatorConnection.HOST_NAME = calculatorProps.getProperty("HOST_NAME");
				CalculatorConnection.PORT_NAME = calculatorProps.getProperty("PORT_NAME");
				CalculatorConnection.MYSQL_USER = calculatorProps.getProperty("MYSQL_USER");
				CalculatorConnection.MYSQL_PASSWORD = calculatorProps.getProperty("MYSQL_PASSWORD");
				CalculatorConnection.SCHEMA_NAME = calculatorProps.getProperty("SCHEMA_NAME");
			} catch (IOException ioex) {
				throw new CalculatorException(1);
			}
		}
		//load the mysql driver
		try {
		   Class.forName("com.mysql.jdbc.Driver").newInstance(); 
		} catch (ClassNotFoundException e) {
			throw new CalculatorException(2);
		}  catch (InstantiationException e) {
			throw new CalculatorException(2);
		} catch (IllegalAccessException e) {
			throw new CalculatorException(2);
		}
		//establish or return connection
		if (calculatorConn == null) {
			try {
				String connectionUrl = "jdbc:mysql://" + HOST_NAME + ":" + PORT_NAME + "/" + SCHEMA_NAME;
				calculatorConn = DriverManager.getConnection(connectionUrl, MYSQL_USER, MYSQL_PASSWORD);
			} catch (SQLException ex) {
				throw new CalculatorException(3);
			}
		}
		return calculatorConn;
	}
	
}
