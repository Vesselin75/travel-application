package calculator;

import java.util.Properties;

/** Application level exception 
 *  Contains system error messages - innitialzation or sql error
 *  Redirects to the error.jsp.
 * 
 * @author Vesselin75
 *
 */
public class CalculatorException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private int errorCode;
	private static final String ERROR_MSG_PREFIX = "System Error: ";
	private static final Properties errorMessages = new Properties();
	
	static {
		errorMessages.put(new Integer(1),  "Unable to load the properties.");
		errorMessages.put(new Integer(2),  "Unable to register driver class.");
		errorMessages.put(new Integer(3),  "Unable to establish connection.");
		
		errorMessages.put(new Integer(4),  "Unable to login user.");
		errorMessages.put(new Integer(5),  "Unable to logout user.");
		
		errorMessages.put(new Integer(6),  "Unable to check username existence.");
		errorMessages.put(new Integer(7),  "Unable to register user.");
				
		errorMessages.put(new Integer(8),  "Unable to check password.");
		
		errorMessages.put(new Integer(9),  "Unable to log operation.");
		errorMessages.put(new Integer(10), "Unable to get log history.");
	}
	
	public CalculatorException(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getMessage() {
		return ERROR_MSG_PREFIX + errorMessages.get(new Integer(errorCode));
	}
	
}
