package calculator.op;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/** Represents a calculator operation
 * 
 *  Contains operation type - 4 types are supported.
 *  Contains the 2 operands.
 *  Contains the timestamp.
 *  Contains the user_id of the user.
 *  The data is stored in the operations table.   
 * 
 * @author Vesselin75
 *
 */
public class CalculatorOp {
	
	/** Id's of the 4 operation types in the operation_types table */
	public static final int ADDITION = 1;
	public static final int SUBTRACTION = 2;
	public static final int MULTIPLICATION = 3;
	public static final int DIVISION = 4;
	
	/** The operation_id of the operation */
	private int operation_id;
	
	/** The id of the user */
	private int userId;	
	
	/** The 2 operands */
	private double operand_1;
	private double operand_2;
	
	/** The operation result */
	private double operationResult;
	
	/** The operation timestamp */
	private Timestamp opTime;
	
	/** The operation type id */
	private int opTypeId;
	
	public int getOperation_id() {
		return operation_id;
	}
	public void setOperation_id(int operation_id) {
		this.operation_id = operation_id;
	}

	public double getOperand_1() {
		return operand_1;
	}
	public void setOperand_1(double operand_1) {
		this.operand_1 = operand_1;
	}

	public double getOperand_2() {
		return operand_2;
	}
	public void setOperand_2(double operand_2) {
		this.operand_2 = operand_2;
	}
	
	public double getOperationResult() {
		return operationResult;
	}
	public void setOperationResult(double operationResult) {
		this.operationResult = operationResult;
	}

	public Timestamp getOpTime() {
		return opTime;
	}
	public void setOpTime(Timestamp opTime) {
		this.opTime = opTime;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getOpTypeId() {
		return opTypeId;
	}
	public void setOpTypeId(int opTypeId) {
		this.opTypeId = opTypeId;
	}

	/** Returns the operation type name
	 * 
	 * @return
	 */
	public String getOperationTypeName() {
		if (opTypeId == 1) {
			return "ADDITION"; 
		}
		if (opTypeId == 2) {
			return "SUBTRACTION"; 
		}
		if (opTypeId == 3) {
			return "MULTIPLICATION"; 
		}
		if (opTypeId == 4) {
			return "DIVISION"; 
		}
		return "";
	}
	
	public String getFormattedTime() {
		SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String formattedTime = format.format(opTime);
		return formattedTime;
	}


}
