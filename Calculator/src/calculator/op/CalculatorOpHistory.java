package calculator.op;

import java.util.HashMap;
import java.util.Iterator;

/** Represents the history of the last operations
 * 
 * @author Vesselin75
 *
 */
public class CalculatorOpHistory {
	
	/** Number of history operations to retrieve and display */
	public static int NUMBER_OF_HISTORY_OPS = 10;
	
	/** Holds the history operations */
	private HashMap<Integer, CalculatorOp> historyOps;
	
	public CalculatorOpHistory() {
		this.historyOps = new HashMap<Integer, CalculatorOp>();
	}
	
	/** Holds the current max key */
	private Integer maxVal = new Integer(0); 
	
	/** Adds an operation */
	public void addOperation(CalculatorOp op) {
		Integer newVal = new Integer(maxVal.intValue() + 1);
		historyOps.put(newVal, op);
		maxVal = newVal;
	}
	
	/** Returns the history operations */
	public HashMap<Integer, CalculatorOp> getOperations() {
		return historyOps;
	}

}
