package calculator.op;

/** Service class for the calculator
 * 
 *  Performs the 4 supported operations - addition, subtraction, multiplication, division
 * 
 * @author Vesselin75
 *
 */
public class Calculator {
	
	/** Addition */	
	public static double addOp(double operand_1, double operand_2) {
		return operand_1 + operand_2;
	}
	
	/** Subtraction */	
	public static double subtractOp(double operand_1, double operand_2) {
		return operand_1 - operand_2;
	}
	
	/** Multiplication */	
	public static double multiplyOp(double operand_1, double operand_2) {
		return operand_1 * operand_2;
	}

	/** Division */	
	public static double divideOp(double operand_1, double operand_2) {
		return operand_1 / operand_2;
	}

	
}
