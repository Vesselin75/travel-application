package calculator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import calculator.op.CalculatorOp;
import calculator.op.CalculatorOpHistory;
import calculator.validation.EmailValidator;

/** Database Facade class.
 *  
 *  Supports user registration.
 *  Supports user login.
 *  Supports user logout.
 *  Supports operation history - logs data as well as retrieves the history.
 * 
 * @author Vesselin75
 *
 */
public class CalculatorFacade {
	
	public static Connection conn;
	
	private static final String SELECT_USERNAME = 
	"select user_id from calculator.users where users.username = ?";
	
	private static final String REGISTER_USER =
	"insert into calculator.users values (null, ?, ?, ?, ?, ?)";		
	
	private static final String SELECT_PASSWORD = 
	"select user_id from calculator.users where users.username = ? and users.password = ?";		
	
	private static final String LOGIN_USER = 
	"update calculator.users set users.logged = true where users.user_id = ?";	
	
	private static final String LOGOUT_USER = 
	"update calculator.users set users.logged = false where users.user_id = ?";
	
	private static final String LOG_OPERATION = 
	"insert into calculator.operations values (null, ?, ?, ?, ?, ?, ?)";		
	
	private static final String GET_HISTORY =
	"select * from calculator.operations where operations.user_id = ? limit ?";	
	
	private static final String GET_USER =
	"select * from calculator.users where users.username = ?";		
	
	
	/** Logs the user and sets the logged flag to true 
	 * 
	 * @param calcUser - user to be logged in
	 * @throws CalculatorException
	 */
	public static void loginUser (CalculatorUser calcUser) throws CalculatorException {
		//set the logged flag
		try {
			PreparedStatement loginUserSmt = conn.prepareStatement(LOGIN_USER);
			loginUserSmt.setInt(1, calcUser.getUserId());
			loginUserSmt.executeUpdate();
		} catch (SQLException sqlex) {
			throw new CalculatorException(4);
		}		
	}
	
	/** Logout the user and sets the logged flag to false 
	 * 
	 * @param calcUser - user to be logged out
	 * @throws CalculatorException
	 */
	public static void logoutUser (CalculatorUser calcUser) throws CalculatorException {
		try {
			PreparedStatement logoutUserSmt = conn.prepareStatement(LOGOUT_USER);
			logoutUserSmt.setInt(1, calcUser.getUserId());
			logoutUserSmt.executeUpdate();
		} catch (SQLException sqlex) {
			throw new CalculatorException(5);
		}		
	}
	
	/** Get user by username
	 * 
	 * @param username
	 * @return
	 * @throws CalculatorException
	 */
	public static CalculatorUser getUser(String username) throws CalculatorException {
		CalculatorUser calculatorUser = new CalculatorUser();
		try {
			PreparedStatement getUserSmt = conn.prepareStatement(GET_USER);
			getUserSmt.setString(1, username);
			ResultSet rs = getUserSmt.executeQuery();
			if (rs.next() == true) {
				int user_id = rs.getInt(1);
				String password = rs.getString(3);
				String fullname = rs.getString(4);
				String email = rs.getString(5);
				boolean logged = rs.getBoolean(6);
				calculatorUser.setUserId(user_id);
				calculatorUser.setUsername(username);
				calculatorUser.setPassword(password);
				calculatorUser.setFullname(fullname);
				calculatorUser.setEmail(email);
				calculatorUser.setLogged(logged);
			} else {
				return null;
			}
		} catch (SQLException sqlex) {
			throw new CalculatorException(4);
		}
		return calculatorUser;
	}
	
	/** Checks that the username is not taken
	 * 
	 * @param calcUser the username for a check
	 * @return
	 * @throws CalculatorException
	 */
	public static boolean checkUsernameExists(String username) throws CalculatorException {
		boolean isTaken = false;
		try {
			PreparedStatement chkUsernameSmt = conn.prepareStatement(SELECT_USERNAME);
			chkUsernameSmt.setString(1, username);
			ResultSet rs = chkUsernameSmt.executeQuery();
			if (rs.next() == true) {
				isTaken = true;	
			}
		} catch (SQLException sqlex) {
			throw new CalculatorException(6);
		}
		return isTaken;
	}

	
	/** Registers a new user
	 * 
	 * @param calcUser - user to be registered
	 * @throws CalculatorException
	 */
	public static void registerUser (CalculatorUser calcUser) throws CalculatorException {
		try {
			PreparedStatement regUserSmt = conn.prepareStatement(REGISTER_USER);
			regUserSmt.setString(1, calcUser.getUsername());
			regUserSmt.setString(2, calcUser.getPassword());
			regUserSmt.setString(3, calcUser.getFullname());
			regUserSmt.setString(4, calcUser.getEmail());	
			regUserSmt.setBoolean(5, false);
			int result = regUserSmt.executeUpdate();
		} catch (SQLException sqlex) {
			throw new CalculatorException(7);
		}
	}	
	
	public static boolean checkPassword(String username, String password) throws CalculatorException {
		//check credentials
		try {
			PreparedStatement checkPasswordSmt = conn.prepareStatement(SELECT_PASSWORD);
			checkPasswordSmt.setString(1, username);
			checkPasswordSmt.setString(2, password);
			ResultSet rs = checkPasswordSmt.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException sqlex) {
			throw new CalculatorException(8);
		}
	}	
	
	
	/** Logs an operation in the database
	 * 
	 * @param calcOp - the operation	  
	 * @throws CalculatorException
	 */
	public static void logOperation (CalculatorOp calcOp) throws CalculatorException {
		try {
			PreparedStatement logOpSmt = conn.prepareStatement(LOG_OPERATION);
			logOpSmt.setInt(1, calcOp.getUserId());
			logOpSmt.setDouble(2, calcOp.getOperand_1());
			logOpSmt.setDouble(3, calcOp.getOperand_2());
			logOpSmt.setDouble(4, calcOp.getOperationResult());
			Timestamp now = new Timestamp(System.currentTimeMillis());
			logOpSmt.setTimestamp(5, calcOp.getOpTime());
			logOpSmt.setInt(6, calcOp.getOpTypeId());
			int result = logOpSmt.executeUpdate();
		} catch (SQLException sqlex) {
			throw new CalculatorException(9);
		}
	}
	
	/** Retrieves the operation history
	 * 
	 * @param calcOpHistory - the history object
	 * @return the history object
	 * @throws CalculatorException
	 */
	public static CalculatorOpHistory getOpHistory (CalculatorUser calcUser) throws CalculatorException {
		CalculatorOpHistory calculatorOpHistory = new CalculatorOpHistory();
		try {
			PreparedStatement getHistorySmt = conn.prepareStatement(GET_HISTORY);
			getHistorySmt.setInt(1, calcUser.getUserId());
			getHistorySmt.setInt(2, CalculatorOpHistory.NUMBER_OF_HISTORY_OPS);
			ResultSet rs = getHistorySmt.executeQuery();
			while (rs.next()) {
			   int operation_id = rs.getInt(1);
			   int user_id = rs.getInt(2);
			   double operand_1 = rs.getDouble(3);
			   double operand_2 = rs.getDouble(4);
			   double operation_result = rs.getDouble(5);
			   Timestamp opTime = rs.getTimestamp(6);
			   int opTypeId = rs.getInt(7);
			   CalculatorOp calculatorOp = new CalculatorOp();
			   calculatorOp.setOperation_id(operation_id);
			   calculatorOp.setUserId(user_id);
			   calculatorOp.setOperand_1(operand_1);
			   calculatorOp.setOperand_2(operand_2);
			   calculatorOp.setOperationResult(operation_result);
			   calculatorOp.setOpTime(opTime);
			   calculatorOp.setOpTypeId(opTypeId);
			   calculatorOpHistory.addOperation(calculatorOp);
			}			
		} catch (SQLException sqlex) {
			throw new CalculatorException(10);
		}
		return calculatorOpHistory;
	}

}
