package calculator;

import java.util.HashMap;

/** Holds the last field values that were entered
 *  Used in case the page is refreshed or the user navigates back. 
 * 
 * @author Vesselin75
 *
 */
public class CalculatorFields {
	
	/** The names of the form fields
	 * 
	 */
	
	public static final String LOGIN_USERNAME = "login_username";
	public static final String LOGIN_PASSWORD = "login_password";
	
	public static final String REGISTER_USERNAME = "register_username";
	public static final String REGISTER_PASSWORD = "register_password";
	public static final String REGISTER_FULLNAME = "fullname";
	public static final String REGISTER_EMAIL = "email";
	
	public static final String CALULATOR_OPERAND1 = "operand_1";
	public static final String CALULATOR_OPERAND2 = "operand_2";
	public static final String CALULATOR_RESULT = "result";
	public static final String CALULATOR_OPERATION_TYPE = "operation_type";
	
	/** The form fields values
	 * 
	 */
	private HashMap<String, String> fieldsValues;
	
	public CalculatorFields() {
		fieldsValues = new HashMap<String, String>();
	}
	
	public String getFieldVal(String key) {
		if (fieldsValues.containsKey(key)) {
			String val = (String)fieldsValues.get(key);
			if (val == null) {
				return "";
			}
			return val;
		} else {
			return "";
		}
	}
	
	public String setFieldVal(String key, String value) {
		return fieldsValues.put(key, value);
	}
	

}
