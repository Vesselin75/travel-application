package com.atom.travel.dao;

import com.atom.travel.calculator.TripBudgetCalculator;
import com.atom.travel.entity.Budget;

public class BudgetOfTripsDAO {

	private TripBudgetCalculator tripBudgetCalculator;
	
	public BudgetOfTripsDAO (TripBudgetCalculator tripBudgetCalculator) {
		this.tripBudgetCalculator = tripBudgetCalculator;
	}
	
	public Budget getTripsBudget () {
		Budget budget = this.tripBudgetCalculator.calculateBudget();
		return budget; 		
	}
	
}
