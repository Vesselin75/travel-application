package com.atom.travel.dao;

import com.atom.travel.calculator.TripBudgetCalculator;
import com.atom.travel.entity.Budget;

public class NumberOfTripsDAO {

	private TripBudgetCalculator tripBudgetCalculator;
	
	public NumberOfTripsDAO (TripBudgetCalculator tripBudgetCalculator) {
		this.tripBudgetCalculator = tripBudgetCalculator;
	}
	
	public String getNumberOfTrips () {
		Budget budget = this.tripBudgetCalculator.calculateBudget();
		String number_of_trips = Integer.toString(budget.getNumberOfTrips());
		return number_of_trips; 		
	}
	
}
