package com.atom.travel.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.atom.travel.controller.TripRequest;
import com.atom.travel.entity.Budget;
import com.atom.travel.entity.Country;
import com.atom.travel.entity.Price;
import com.atom.travel.entity.Trip;

public class TripBudgetCalculator {
	
	@PersistenceContext(unitName = "pu")
    private EntityManager entityManager;
	
	private static int number_of_countries = 5;
	
	private float budget_per_country;
	private float leftover;
	private float total_budget;
	private int base_currency_id;
	private int starting_country_id;
	
	private Budget budget;
	
	@Autowired	
	public TripBudgetCalculator (TripRequest tripRequest) {		
		this.base_currency_id = tripRequest.getBaseCurrencyId();
		this.total_budget = tripRequest.getTotalBudget();
		this.budget_per_country = tripRequest.getBudgetPerCountry();		
		this.starting_country_id = tripRequest.getStartingCountryId();
		this.budget = new Budget (1, new HashMap<Price, Trip> ()); 
	}
	
	public Budget calculateBudget () {
		this.leftover = total_budget - (budget_per_country * number_of_countries);
		float actual_budget = total_budget - leftover;
		int number_of_trips = (int) ((int)actual_budget/budget_per_country);
		ArrayList<Country> neighbourCountries = this.getNeighbourCountries(starting_country_id);
		int number_of_neighbour_countries = neighbourCountries.size();
		ArrayList<Country> tripCountries = new ArrayList<Country>();
		for (int i = 1; i <= number_of_trips; i++) {
			if (i == starting_country_id) continue;
			tripCountries.add(neighbourCountries.get(i));
		}
		ArrayList<String> currencySymbols = this.getAllCurrencySymbols(starting_country_id);
		ArrayList<String> currencyRates = this.getAllCurrencyRates(base_currency_id, starting_country_id);
		for (int j = 1; j <= number_of_trips; j++) { 
			float currentRate = Float.parseFloat( currencyRates.get(j) );
			Price currentPrice = new Price (j, currentRate * budget_per_country );
			Country currentCountry = tripCountries.get(j);
			Trip currentTrip = new Trip (j, currentCountry);
			this.budget.addTrip(currentPrice, currentTrip);		
			
		}		
		return this.budget;
	}
	
	public ArrayList<Country> getNeighbourCountries (int starting_country_id) {
		//get all countries
		@SuppressWarnings("unchecked")
		ArrayList<Country> neighbourCountries =  (ArrayList<Country>) entityManager.createNamedQuery("findAllTripCountries")		
			    .setParameter("startingCountryId", Integer.toString(starting_country_id))
			    .getResultList();
		return neighbourCountries;		
	}
	
	public ArrayList<String> getAllCurrencySymbols (int starting_country_id) {
		//get all currency symbols
		@SuppressWarnings("unchecked")
		ArrayList<String> currencySymbols =  (ArrayList<String>) entityManager.createNamedQuery("findAllCurrencySymbols")		
		    .setParameter("startingCountryId", Integer.toString(starting_country_id))
		    .getResultList();
		return currencySymbols;		
	}
	
	public ArrayList<String> getAllCurrencyRates (int base_currency_id, int starting_country_id) {
		//get all currency rates
		@SuppressWarnings("unchecked")
		ArrayList<String> currencyRates =  (ArrayList<String>) entityManager.createNamedQuery("findAllCurrencyRates")		
			.setParameter("baseCurrencyId", Integer.toString(base_currency_id))
			.setParameter("startingCountryId", Integer.toString(starting_country_id))
		    .getResultList();
		return currencyRates;			
	}
	
	

}
