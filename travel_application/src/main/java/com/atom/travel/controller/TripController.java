package com.atom.travel.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atom.travel.calculator.TripBudgetCalculator;
import com.atom.travel.dao.BudgetOfTripsDAO;
import com.atom.travel.dao.NumberOfTripsDAO;
import com.atom.travel.entity.Budget;

@RestController
@RequestMapping(path = "/trips")
public class TripController {

	@Autowired
    private NumberOfTripsDAO numberOfTripsDAO;
	
	@Autowired
    private BudgetOfTripsDAO budgetOfTripsDAO;
	
	@RequestMapping(value="/number/{trip_data}", method=RequestMethod.GET, produces="application/json")
	public String getNumberOfTrips(@PathParam("trip_data") String trip_data) 
	{
		TripRequest tripRequest = new TripRequest (trip_data);
		TripBudgetCalculator tripBudgetCalculator = new TripBudgetCalculator (tripRequest);
		numberOfTripsDAO = new NumberOfTripsDAO (tripBudgetCalculator);  
		
        return numberOfTripsDAO.getNumberOfTrips();
    }
	
	@RequestMapping(value="/budget/{trip_data}", method=RequestMethod.GET, produces="application/json")
	public Budget getTripBudget(@PathParam("trip_data") String trip_data) 
    {
		TripRequest tripRequest = new TripRequest (trip_data);
		TripBudgetCalculator tripBudgetCalculator = new TripBudgetCalculator (tripRequest);
		budgetOfTripsDAO = new BudgetOfTripsDAO (tripBudgetCalculator);  
		
        return budgetOfTripsDAO.getTripsBudget();
    }
	
}
