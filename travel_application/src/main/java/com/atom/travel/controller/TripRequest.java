package com.atom.travel.controller;


import java.util.StringTokenizer;

public class TripRequest {

	private int starting_country_id;
	private int budget_per_country;
	private int total_budget;
	private int base_currency_id;
	
	public TripRequest (String trip_data) {
		StringTokenizer tzer = new StringTokenizer ("'");
		while (tzer.hasMoreTokens()) {
			this.starting_country_id = Integer.parseInt(tzer.nextToken());
			this.budget_per_country = Integer.parseInt(tzer.nextToken());
			this.total_budget = Integer.parseInt(tzer.nextToken());
			this.base_currency_id = Integer.parseInt(tzer.nextToken());
		}
	}
	
	public int getStartingCountryId () {
		return this.starting_country_id;
	}
	public void setStartingCountryId (int starting_country_id) {
		this.starting_country_id = starting_country_id;
	}
	
	public int getBudgetPerCountry() {
		return this.budget_per_country;
	}
	public void setBudgetPerCountry (int budget_per_country) {
		this.budget_per_country = budget_per_country;
	}
	
	public int getTotalBudget () {
		return this.total_budget;
	}
	public void setTotalBudget (int total_budget) {
		this.total_budget = total_budget;
	}
	
	public int getBaseCurrencyId () {
		return this.base_currency_id;
	}
	public void setBaseCurrencyId (int base_currency_id) {
		this.base_currency_id = base_currency_id;
	}
	
}
