package com.atom.travel.entity;

public class Price {
	
	private int currency_id; 
	private float value;
	
	public Price (int currency_id, float value) {
		this.currency_id = currency_id;
		this.value = value;
	}
	
	public int getCurrencyId () {
		return this.currency_id;
	}
	public void setCurrencyId (int currency_id) {
		this.currency_id = currency_id;
	}
	
	public float getValue () {
		return this.value;
	}
	public void setValue (float value) {
		this.value = value;
	}


}
