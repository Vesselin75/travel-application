package com.atom.travel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/** Describes and exchange currency - 6 types
 * 
 * @author Vesselin75
 *
 */
@Entity
@Table(name="travelschema.T_EXCHANGE_CURRENCY")
@NamedQuery(
	    name="findAllCurrencySymbols",
	    query="SELECT c FROM CurrencyExchange c WHERE c.currency_id NOT LIKE :startingCountryId"
)
public class CurrencyExchange {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="currency_id")
	private int currency_id;
	
	@Column(name = "currency_symbol") 
	private String currency_symbol;
	
	public CurrencyExchange (int currency_id, String currency_symbol ){
		this.currency_id = currency_id;
		this.currency_symbol = currency_symbol;
	}
	
	public int getCurrencyExchangeId () {
		return this.currency_id;
	}
	public void setCurrencyExchangeId (int currency_id) {
		this.currency_id = currency_id;
	}
	
	public String getCurrencyExchangeSymbol () {
		return this.currency_symbol;
	}
	public void setCurrencyExchangeSymbol (String currency_symbol) {
		this.currency_symbol = currency_symbol;
	}
	

}
