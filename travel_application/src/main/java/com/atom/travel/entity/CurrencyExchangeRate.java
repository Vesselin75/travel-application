package com.atom.travel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** Describes a currency exchange rate
 * 
 * @author Vesselin75
 *
 */
@Entity
@Table(name="travelschema.T_EXCHANGE_RATE")
@NamedQuery(
	    name="findAllCurrencyRates",
	    query="SELECT c FROM CurrencyExchangeRate c " + 
	    	  "WHERE c.base_currency_id = :baseCurrencyId " + 
	    	  "AND c.currency_symbol_id NOT LIKE :startingCountryId"
)
public class CurrencyExchangeRate {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "rate_id") 
	private int rate_id;
	
	@Column(name = "base_currency_id") 
	private int base_currency_id;
	
	@Column(name = "currency_symbol_id") 
	private int currency_symbol_id;
	
	@Column(name = "currency_rate") 
	private float currency_rate;
	
	public CurrencyExchangeRate (int rate_id, int base_currency_id,int currency_symbol_id, float currency_rate) {
		this.rate_id = rate_id;
		this.base_currency_id = base_currency_id;
		this.currency_symbol_id = currency_symbol_id;
		this.currency_rate = currency_rate;
	}
	
	public int getCurrencyExchangeRateId () {
		return this.rate_id;
	}
	public void setCurrencyExchangeRateId (int rate_id) {
		this.rate_id = rate_id;
	}
	
	public int getBaseCurrencyId () {
		return this.base_currency_id;
	}
	public void setBaseCurrencyId (int base_currency_id) {
		this.base_currency_id = base_currency_id;
	}
	
	public int getCurrencySymbolId () {
		return this.currency_symbol_id;
	}
	public void setCurrencySymbolId (int currency_symbol_id) {
		this.currency_symbol_id = currency_symbol_id;
	}
	
	public float getCurrencyRate () {
		return this.currency_rate;
	}
	public void setCurrencyRate (float currency_rate) {
		this.currency_rate = currency_rate;
	}
	

}
