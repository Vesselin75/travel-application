package com.atom.travel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/** Describes a base currency - EUR or USD or other
 * 
 * @author Vesselin75
 *
 */
@Entity
@Table(name="travelschema.T_EXCHANGE_BASE")
public class CurrencyExchangeBase {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ex_base_id")
	private int ex_base_id;
	
	@Column(name = "ex_base_symbol") 
	private String ex_base_symbol;
	
	public CurrencyExchangeBase (int ex_base_id, String ex_base_symbol ){
		this.ex_base_id = ex_base_id;
		this.ex_base_symbol = ex_base_symbol;
	}
	
	public int getCurrencyExchangeBaseId () {
		return this.ex_base_id;
	}
	public void setCurrencyExchangeBaseId (int ex_base_id) {
		this.ex_base_id = ex_base_id;
	}
	
	public String getCurrencyExchangeBaseSymbol () {
		return this.ex_base_symbol;
	}
	public void setCurrencyExchangeBaseSymbol (String ex_base_symbol) {
		this.ex_base_symbol = ex_base_symbol;
	}
	
}
