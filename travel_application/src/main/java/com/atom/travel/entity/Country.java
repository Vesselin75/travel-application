package com.atom.travel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** Describes a country
 * 
 * @author Vesselin75
 *
 */
@Entity
@Table(name="travelschema.T_COUNTRY")
@NamedQuery(
	    name="findAllTripCountries",
	    query="SELECT c FROM Country c WHERE c.country_id NOT LIKE :startingCountryId"
)
public class Country {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	@Column(name="country_id")
	private int country_id;
	
	@Column(name = "country_code")  
	private String country_code;
	
	@Column(name = "country_name")   
	private String country_name;
	
	public Country (int country_id, String country_code, String country_name) {
		this.country_id = country_id;
		this.country_code = country_code;
		this.country_name = country_name;		
	}
	
	
	public int getCountryId () {
		return this.country_id;
	}
	public void setCountryId (int country_id) {
		this.country_id = country_id;
	}
	
	public String getCountryCode () {
		return this.country_code;
	}
	public void setCountryCode (String country_code) {
		this.country_code = country_code;
	}

	public String getCountryName () {
		return this.country_name;
	}
	public void setCountryName (String country_name) {
		this.country_name = country_name;
	}

	
	
}
