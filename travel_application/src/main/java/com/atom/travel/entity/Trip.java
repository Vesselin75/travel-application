package com.atom.travel.entity;

import java.util.ArrayList;

public class Trip {
	
	private int trip_id;
	private Country country;
	
	public Trip (int trip_id, Country country) {
		this.trip_id = trip_id;
		this.country = country;
	}
	
}
