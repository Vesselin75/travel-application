package com.atom.travel.entity;

import java.util.HashMap;

public class Budget {
	
	private int budget_id;
	private int number_of_trips = 0; 
	private HashMap<Price, Trip> tripPricesMap;	
	
	public Budget (int budget_id, HashMap<Price, Trip> tripPricesMap) {
		this.budget_id = budget_id;
		this.tripPricesMap = new HashMap<Price, Trip> ();
	}
	
	public void addTrip (Price price, Trip trip) {
		this.tripPricesMap.put(price, trip);
	}
	
	public void clearData () {
		this.tripPricesMap = new HashMap<Price, Trip> ();
	}
	
	public int getNumberOfTrips () {
		return this.tripPricesMap.size();
	}
	public void setNumberOfTrips (int number_of_trips) {
		this.number_of_trips = number_of_trips;
	}
	

}
