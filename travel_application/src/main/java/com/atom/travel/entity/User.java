package com.atom.travel.entity;

public class User {
	
	private int user_id;
	private String user_name;
	private String user_password;
	
	public User (int user_id, String user_name, String user_password) {
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_password = user_password;		
	}
	
	public int getUserId () {
		return this.user_id;
	}
	public void setUserId (int user_id) {
		this.user_id = user_id;
	}
	
	public String getUserName () {
		return this.user_name;
	}
	public void setUserName (String user_name) {
		this.user_name = user_name;
	}

	public String getUserPassword () {
		return this.user_password;
	}
	public void setUserPassword (String user_password) {
		this.user_password = user_password;
	}

}
