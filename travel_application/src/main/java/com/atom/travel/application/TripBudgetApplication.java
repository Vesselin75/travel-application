package com.atom.travel.application;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
 
@SpringBootApplication
public class TripBudgetApplication {
	 
    public static void main(String[] args) {
    	
		SpringApplication.run(TripBudgetApplication.class, args);
    }
}

