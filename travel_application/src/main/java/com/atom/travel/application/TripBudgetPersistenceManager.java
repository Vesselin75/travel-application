package com.atom.travel.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

public class TripBudgetPersistenceManager {
	
	@Autowired
	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory()
	{
	    LocalContainerEntityManagerFactoryBean lEMF =  new LocalContainerEntityManagerFactoryBean();
	    lEMF.setPersistenceUnitName("tripBudgetPU");
	    lEMF.setPersistenceXmlLocation("persistence.xml");
	    return lEMF;
	}

}
